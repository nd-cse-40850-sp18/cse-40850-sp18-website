title:      "Reading 01: Early History"
icon:       fa-book
navigation: []
internal:
external:
body:       |

  **Everyone**:

  The readings for this week revolve around the [early history] of video games,
  which is "the period of time between the first interactive electronic game
  with an electronic display in 1947, the first true video games in the early
  1950s, and the rise of early arcade video games in the 1970s (Pong and the
  beginning of the first generation of video game consoles with the Magnavox
  Odyssey, both in 1972)".

  Later in the week, we will discuss [emulators] such as [MAME] as well as
  [DOSBox].

  [MAME]:           http://mamedev.org/index.php
  [DOSBox]:         https://www.dosbox.com/
  [early history]:  https://en.wikipedia.org/wiki/Early_history_of_video_games
  [emulators]:      https://en.wikipedia.org/wiki/Emulator

  ## Readings

  The following articles highlight key milestones in the development of video
  games.  The first article provides an overview of this time period while the
  subsequent articles focus on particular events or artifacts from the time
  period.

  - [The Father of the Video Game: The Ralph Baer Prototypes and Electronic
    Games](http://americanhistory.si.edu/collections/object-groups/the-father-of-the-video-game-the-ralph-baer-prototypes-and-electronic-games/video-game-history)

  ### NIMROD

  - [Nimrod, the World's First Gaming Computer](https://www.wired.com/2010/06/replay/)

  - [Ferranti NIMROD](http://www.goodeveca.net/nimrod/)

  ### Tennis for Two

  - [The First Video Game?](https://www.bnl.gov/about/history/firstvideo.php)

  - [The anatomy of the first video game](http://www.nbcnews.com/id/27328345/ns/technology_and_science-games/t/anatomy-first-video-game/#.WmJbMB3qthE)

  ### Spacewar!

  - [The History of Spacewar!: The Best Waste of Time in the History of the Universe](https://www.gamasutra.com/view/feature/132438/the_history_of_spacewar_the_best_.php)

  - [S P A C E W A R - Fanatic Life and Symbolic Death Among the Computer Bums](http://wheels.org/spacewar/stone/rolling_stone.html)

      - [Spacewar! Emulator](http://www.masswerk.at/spacewar/)
      - [Spacewar! Emulator](http://spacewar.oversigma.com/)

  ### Pong

  - [Atari Teenage Riot: The Inside Story Of Pong And The Video Game Industry's Big Bang](https://www.buzzfeed.com/chrisstokelwalker/atari-teenage-riot-the-inside-story-of-pong-and-t?utm_term=.ct5o36mMp#.msGx20rMA)

  - [Pong, Atari, and the origins of the home video game](http://americanhistory.si.edu/blog/2012/04/pong-atari-and-the-origins-of-the-home-video-game.html)

  - [PONG-Story](http://www.pong-story.com)

  ### Magnavox Odyssey

  - [Magnavox Odyssey retrospective: How console gaming was born](http://www.digitalspy.com/gaming/retro-gaming/feature/a616235/magnavox-odyssey-retrospective-how-console-gaming-was-born/)

  - [Inside the Magnavox Odyssey, the First Video Game Console](https://www.pcworld.com/article/256101/inside_the_magnavox_odyssey_the_first_video_game_console.html)

  - [Online Odyssey Museum](http://www.magnavox-odyssey.com/)

  ## Reflection

  For this upcoming week, you are to consider the following questions as you
  perform the readings, discuss in class, and play some old games:

  1. How did the development of video gaming come about and what impact did it
  have on early computing?

      - What technological advancements enabled the development of video gaming?

      - What technological obstacles hindered the medium?

  2. What was the nature of the early video games?

      - What do these early video games have in common with modern video games?

      - How are these early video games different form modern video games?

  3. What was the nature of early computing machines or gaming consoles?

      - What do these early machines have in common with modern gaming
        platforms?

      - How are these early machines different from modern gaming platforms?

  Note, you should not simply list the questions and answer each one directly.
  Instead, the questions are there to help you brainstorm about the question:

  > What unique opportunities and challenges did early video gaming present to
  both developers and players?

  Your individual reflection should ultimately answer this question.
