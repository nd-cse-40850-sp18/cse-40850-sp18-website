title:      "Reading 05: Xbox, Wii, Rhythm"
icon:       fa-book
navigation: []
internal:
external:
body:       |

  **Everyone**:

  Next week, we will discuss the [2000s in video gaming], where we see the
  advent of modern consoles such as [Xbox 360], [Nintendo Wii], [PlayStation
  3], and unique peripherals such as [Wiimotes] and the [Kinect].  In fact,
  this era saw the rise and fall of the [rhythm game] fad, where video gaming
  went from a physically passive experience to one that required more full
  bodied movement.

  [2000s in video gaming]: https://en.wikipedia.org/wiki/2000s_in_video_gaming
  [Xbox 360]:       https://en.wikipedia.org/wiki/Xbox_360
  [Nintendo Wii]:   https://en.wikipedia.org/wiki/Wii
  [PlayStation 3]:  https://en.wikipedia.org/wiki/PlayStation_3
  [Wiimotes]:       https://en.wikipedia.org/wiki/Wii_Remote
  [Kinect]:         https://en.wikipedia.org/wiki/Kinect
  [Rhythm game]:    https://en.wikipedia.org/wiki/Rhythm_game

  ## Readings

  For this week, you are to read the following articles:

  ### Xbox

  - [A Brief History of Xbox](http://in.ign.com/xbox-one/64868/feature/a-brief-history-of-xbox)

  - [The History of the Xbox](https://www.digitaltrends.com/gaming/the-history-of-the-xbox/)

  - [Kinect Hackers Are Changing the Future of Robotics](https://www.wired.com/2011/06/mf_kinect/)

  - [Microsoft has stopped making the Kinect, and that makes me sad](https://arstechnica.com/gaming/2017/10/microsoft-has-stopped-making-the-kinect-and-that-makes-me-sad/)

  ### Wii

  - [The Big Ideas Behind Nintendo's Wii](https://web.archive.org/web/20071015021224/http://www.businessweek.com/technology/content/nov2006/tc20061116_750580.htm)

  - [Wii Controllers: Unlocking the Secrets](http://www.ign.com/articles/2006/07/15/wii-controllers-unlocking-the-secrets)

  - [Attack of the 'Wiimote' Hacks](https://www.wired.com/2006/12/attack-of-the-wiimote-hacks/)

  - [Of gyroscopes and gaming: the tech behind the Wii MotionPlus](https://arstechnica.com/gaming/2008/08/wii-motion-sensor/)

  ### Rhythm

  - [Roots of rhythm: a brief history of the music game genre](https://arstechnica.com/gaming/2009/03/ne-music-game-feature/)

  - [Music Games Aren’t Dead, Just Waiting to Be Reborn](https://www.wired.com/2009/11/music-game-sales/)

  - [Rigopulos: Don't blame Activision for rhythm bubble burst](https://www.gamesindustry.biz/articles/2014-05-19-rigopulos-dont-blame-activision-for-rhythm-bubble-burst)

  - [P.E. Classes Turn to Video Game That Works Legs](http://www.nytimes.com/2007/04/30/health/30exer.html)

  ### Video Games

  After doing some reading, you should play some games from the following
  the [Notable video-game franchises established in the 2000s] list such as:

  - [Angry Birds](https://en.wikipedia.org/wiki/Angry_Birds)

  - [Assassin's Creed](https://en.wikipedia.org/wiki/Assassin%27s_Creed)

  - [Bejeweled](https://en.wikipedia.org/wiki/Bejeweled_(series))

  - [Call of Duty](https://en.wikipedia.org/wiki/Call_of_Duty)

  - [Club Penguin](https://en.wikipedia.org/wiki/Club_Penguin)

  - [Counter-Strike](https://en.wikipedia.org/wiki/Counter-Strike)

  - [Guitar Hero](https://en.wikipedia.org/wiki/Guitar_Hero)

  - [LittleBigPlanet](https://en.wikipedia.org/wiki/LittleBigPlanet)

  - [Rock Band](https://en.wikipedia.org/wiki/Rock_Band)

  - [Star Wars: Knights of the Old Republic](https://en.wikipedia.org/wiki/Star_Wars:_Knights_of_the_Old_Republic_(series))

  - [Wii Sports](https://en.wikipedia.org/wiki/Wii_Sports)

  You many also play any game that falls under the categories of [virtual
  reality] or [augmented reality].

  As you play a game, do some research on the history behind the game and then
  summarize why the game is so notable.

  [Notable video-game franchises established in the 2000s]: https://en.wikipedia.org/wiki/2000s_in_video_gaming#Notable_video-game_franchises_established_in_the_2000s
  [Rock Band]:  https://en.wikipedia.org/wiki/Rock_Band

  ## Reflection

  For this upcoming week, you are to consider the following question as you
  perform the readings, discuss in class, and play some games:

  > Video games have historically been physically passive experiences (ie.
  players sit and interact via stationary input devices).  With devices such as
  the [Kinect] and [Wiimotes] and rhythm games such as [Rock Band], there has
  been a push for more physically demanding and interactive games.  What do you
  make of these types of games?  Do you enjoy them or do you prefer old fashion
  passive gaming?  What about the potential for things like [virtual reality]
  or even [augmented reality]?

  [virtual reality]:    https://en.wikipedia.org/wiki/Virtual_reality
  [augmented reality]:  https://en.wikipedia.org/wiki/Augmented_reality

  Your individual reflection should provide short reviews of the games you
  played, provided screenshots of gameplay, and ultimately answer this question.
