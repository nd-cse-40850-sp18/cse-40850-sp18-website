title:      "Project 02: Modern Computing"
icon:       fa-gamepad
navigation: []
internal:
external:
body:       |

  **Everyone**:

  For the second project, you are to work in groups of **2** - **4** students
  to create a game or related artifact for a **modern computing platform**.
  What this means is that the software you develop should involve modern
  computer graphics, networking, mobile computing, or artificial intelligence.

  ## Requirements

  Your group's project artifact must meet the following requirements:

  1. The artifact must utilize a modern graphics library (ie. [OpenGL],
  [DirectX], etc.], game engine (ie. [Unity], [Cocos 2D], etc.), or animation
  software (ie. [Maya], [Blender], etc.).

  2. The artifact should be interactive and capable of being manipulated by a
  user via some sort of input device (unless it is an animated movie or an
  artificial intelligence application).

  3. The artifact must run in on a modern computing platform (ie. [Windows],
  [macOS], [Linux], [Android], [iOS], [XBox], etc.).

  4. The artifact's source code is stored on an online repository such as
  [GitHub], [GitLab], or [Bitbucket].

  5. The project includes sufficient documentation to build, deploy, and use
  the artifact.

  6. The project has a **website** that presents the artifact in an aesthetic
  manner, explains how the project it takes advantage of the chosen modern
  computing platform, and includes a video of the artifact in action.

  [OpenGL]:     https://www.opengl.org/
  [DirectX]:    https://msdn.microsoft.com/en-us/library/windows/desktop/ee663274(v=vs.85).aspx
  [Unity]:      https://unity3d.com/
  [Cocos 2D]:   http://www.cocos2d.org/
  [Maya]:       https://www.autodesk.com/products/maya/overview
  [Blender]:    https://www.blender.org/

  [Windows]:    https://www.microsoft.com/en-us/windows
  [macOS]:      https://www.apple.com/macos/
  [Linux]:      https://www.kernel.org/
  [Android]:    https://www.android.com/
  [iOS]:        https://www.apple.com/ios/
  [XBox]:       https://www.xbox.com/en-US/

  <div class="alert alert-warning" markdown="1">
  #### <i class="fa fa-code"></i> No Man's Sky is the Limit

  The purpose of the project is to provide you an opportunity to experience new
  and modern computing technologies.  Because of this, you should avoid things
  you have previously seen such as [Python] and [PyGame].  Moreover, you should
  avoid doing everything yourself, and try to take advantage of as many
  libraries and frameworks as possible to create a polished product.

  That said, the artifact, platform, and tools are mostly up to you; use this
  opportunity to explore something interesting to you.

  If in doubt, ask the instructor if you can utilize a particular tool or
  library.

  </div>

  ## Ideas

  Here are some possible project ideas:

  1. Convert a classic 2D game into a 3D game.

  2. Port a classic game to a mobile or web platform.

  3. Create a short animated film (around 30 seconds to a minute).

  4. Construct a game engine or framework for making a particular type of game.

  5. Implement a significant mod for a current game.

  6. Program a sophisticated bot for a current game.

  7. Write a program that simulates or visualizes interesting scientific data.

  8. Create a competitive multi-player networked game.

  9. Develop a social game for the [Chromecast].

  10. Utilize the [Kinect] to make a demo or game.

  11. Make a virtual reality demo or game.

  12. Significant improve an existing open source game such as [0 AD] or [The Battle for Wesnoth](http://www.wesnoth.org/).

  [Chromecast]: https://www.chromecast.com/
  [0 AD]:       https://play0ad.com/

  ## Timeline

  Here is the project timeline:

  <table class="table table-bordered table-striped">
    <thead>
      <th>Date</th>
      <th>Milestone</th>
      <th>Description</th>
    </thead>
    <tbody>
      <tr>
        <td>March 31</td>
        <td>Proposal</td>
        <td>Description of proposed project.</td>
      </tr>
      <tr>
        <td>April 24</td>
        <td>Progress</td>
        <td>Status report of current progress.</td>
      </tr>
      <tr>
        <td>May 1</td>
        <td>Sprint</td>
        <td>In-class sprints.</td>
      </tr>
      <tr>
        <td>May 9</td>
        <td>Presentations</td>
        <td>In-class presentations and demonstrations.</td>
      </tr>
    </tbody>
  </table>

  More details about each of these milestones is described below.

  ## Proposal

  Your group must submit a proposal document by **noon** on **Saturday,
  March 31**, which provides the following information:

  1. Lists the group members and each of their proposed roles.

  2. Describe the proposed project and how it meets the project requirements.

  3. Enumerates any resources required.

  <div class="alert alert-success" markdown="1">
  #### <i class="fa fa-money"></i> Hardware Resources

  If you need any hardware resources please let the instructor know and he will
  see if he can procure your requests.  Please do this sooner rather than
  later.

  </div>

  ## Progress

  Your group must submit a proposal **slide deck** (*5-8 slides*) by **noon**
  on **Saturday, April 21**, which provides the following information:

  1. Addresses the questions and expections in feedback email provided
  regarding the project proposal.

  2. Summarizes the work done this far, in particular the design and
  implementation details of your artifact.

  3. Enumerates the remaining tasks, with specifics on what you plan on
  accomplishing during the upcoming sprint week and what you realistically plan
  on accomplishing before the deadline.

  4. Itemize contributions to the project thus far and what how each member
  will aid in completing the tasks above.

  If possible, provide **video** or **image** evidence of your project and your
  progress.

  ## Presentation

  Your group must submit a link to their project **website** by **midnight** on
  **Tuesday, May 8**, which provides the following information:

  1. Overview of your project and how you fits into the required theme (and
  requirements).

  2. Description of the design and implementation of the artifact (and how each
  person contributed), focusing on the key concepts and ideas.

  3. Summary of what was accomplished and what was learned.

  4. A demonstration of the artifact.

  Each group should plan for about **5** minutes for the presentation and **2**
  minutes for questions.   Towards the end of the presentation day, there will
  be about **15** minutes for students to go around the room and test out the
  projects presented that day.

  ## Rubric

  The project will be graded with the following rubric:

  <table class="table table-bordered table-striped">
    <thead>
      <th>Metric</th>
      <th>Points</th>
      <th>Description</th>
    </thead>
    <tbody>
      <tr>
        <td>Proposal</td>
        <td>10</td>
        <td>Does the proposed project meet the requirements?</td>
      </tr>
      <tr>
        <td>Progress</td>
        <td>10</td>
        <td>How much work did the group do before the sprint?</td>
      </tr>
      <tr>
        <td>Presentation</td>
        <td>10</td>
        <td>How well did the group present and demonstrate their artifact?</td>
      </tr>
      <tr>
        <td>Deliverable - Requirements</td>
        <td>10</td>
        <td>How well did the project meet the requirements?</td>
      </tr>
      <tr>
        <td>Deliverable - Technical Challenge</td>
        <td>10</td>
        <td>How difficult was the project?</td>
      </tr>
      <tr>
        <td>Deliverable - Execution</td>
        <td>10</td>
        <td>How complete was the project?.</td>
      </tr>
      <tr>
        <td>Deliverable - User Experience</td>
        <td>10</td>
        <td>How enjoyable was project for end users?</td>
      </tr>
    </tbody>
  </table>

  <div class="alert alert-danger" markdown="1">
  #### <i class="fa fa-gavel"></i> Peer Evaluation

  The first two metrics will be graded by the instructor, but the final five
  metrics will be evaluated based on feedback from your fellow classmates.
  </div>

  ## Deliverables

  Once you have organized your group, please fill out the following
  [form](https://goo.gl/forms/OS9CLYZl3IDlWtQ92)

  <div class="text-center">
      <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfZ_Mh7RmwoJe5NDctWnkwpLLdPM13QdbR2gDCeTA2xYedfjw/viewform?embedded=true" width="760" height="500" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>
  </div>

  You can update your submission whenever you have your proposal, code
  repository, and presentation slides.

  [Python]:         https://www.python.org/
  [PyGame]:         http://www.pygame.org/
  [GitHub]:         https://github.com
  [GitLab]:         https://gitlab.com
  [Bitbucket]:      https://bitbucket.org
  [Kinect]:         https://www.xbox.com/en-US/xbox-one/accessories/kinect
