title:      "Project 01: Retro Computing"
icon:       fa-gamepad
navigation: []
internal:
external:
body:       |

  **Everyone**:

  For the first project, you are to work in groups of **2** - **4** students to
  create a game or related artifact for a **resource constrained environment**.
  What this means is that the software you develop should run in an environment
  with low resources (CPU, memory, disk, etc.) such as an [Arduino], a
  [Raspberry Pi], or an [emulator] such as [DOSBox].

  ## Requirements

  Your group's project artifact must meet the following requirements:

  1. The artifact is grounded in some element of video game history.

  2. The artifact is interactive and is capable of being manipulated by a user
  via some sort of input device.

  3. The artifact must run in a resource constrained environment.

  4. The artifact's source code is stored on an online repository such as
  [GitHub], [GitLab], or [Bitbucket].

  5. The project includes sufficient documentation to build, deploy, and use
  the artifact.

  6. The project `README.md` must explain the project and its context in terms
  of video game history (between 250 - 500 words).

  <div class="alert alert-warning" markdown="1">
  #### <i class="fa fa-code"></i> Keeping it Old School

  Because the purpose of the project is to provide you an opportunity to
  experience what it was like to program on old (ie. resource constrained)
  computing system, you should avoid using modern programming languages and
  libraries such as [Python] and [PyGame].  Instead, you should focus on [C],
  [C++], or even [assembly] and render your graphics directly.

  That said, the artifact, platform, and tools is mostly up to you; use this
  opportunity to explore something interesting to you.

  If in doubt, ask the instructor if you can utilize a particular tool or
  library.

  </div>

  ## Ideas

  Here are some possible project ideas:

  1. Re-create a classic game such as [SpaceWar], [Pong], [Pac-Man],
  [Breakout], [Frogger], etc.

  2. Write a text-based MUD such as [Zork], [Adventure], [NetHack], or [Rogue].

  3. Use an old language like [QBasic] to make a game like [Nibbles] or
  [Gorillas].

  4. Build a machine emulator for the [Z80], [8086], [M68K] or any other old
  architecture.

  5. Construct a game out of logic gates (perhaps with a [FPGA] board).

  6. Develop a game an [Arduino] kit such as the [Arduboy].

  7. Mod an old game like [DOOM].

  8. Create programming tools like a tile editor, sprite editor, or level
  editor.

  9. Design a collection of game artwork such as sprites and tiles.

  10. Write a program that generates/plays music (perhaps [MIDI]).

  11. Create a scientific visualization.

  12. Develop a [BBS].

  ## Timeline

  Here is the project timeline:

  <table class="table table-bordered table-striped">
    <thead>
      <th>Date</th>
      <th>Milestone</th>
      <th>Description</th>
    </thead>
    <tbody>
      <tr>
        <td>February 3</td>
        <td>Proposal</td>
        <td>Description of proposed project.</td>
      </tr>
      <tr>
        <td>February 27</td>
        <td>Progress</td>
        <td>Status report of current progress.</td>
      </tr>
      <tr>
        <td>February 27, March 1</td>
        <td>Sprints</td>
        <td>In-class sprints.</td>
      </tr>
      <tr>
        <td>March 6, March 8</td>
        <td>Presentations</td>
        <td>In-class presentations and demonstrations.</td>
      </tr>
    </tbody>
  </table>

  More details about each of these milestones is described below.


  ## Proposal

  Your group must submit a proposal document by **noon** on **Saturday,
  February 3**, which provides the following information:

  1. Lists the group members and each of their proposed roles.

  2. Describe the proposed project and how it meets the project requirements.

  3. Enumerates any resources required.

  <div class="alert alert-success" markdown="1">
  #### <i class="fa fa-money"></i> Hardware Resources

  If you need any hardware resources such as an [Arduboy] or a [Raspberry Pi],
  please let the instructor know and he will see if he can procure your
  requests.  Please do this sooner rather than later.

  </div>

  ## Progress

  Your group must submit a proposal **slide deck** (*5-8 slides*) by
  **midnight** on **Tuesday, February 27**, which provides the following
  information:

  1. Addresses the questions and expections in feedback email provided
  regarding the project proposal.

  2. Summarizes the work done this far, in particular the design and
  implementation details of your artifact.

  3. Enumerates the remaining tasks, with specifics on what you plan on
  accomplishing during the upcoming sprint week and what you realistically plan
  on accomplishing before the deadline.

  4. Itemize contributions to the project thus far and what how each member
  will aid in completing the tasks above.

  If possible, provide **video** or **image** evidence of your project and your
  progress.

  ## Presentation

  Your group must submit a project **slide deck** (*5-8 slides*) by
  **midnight** on **Monday, March 5**, which provides the following
  information:

  1. Overview of your project and how you fits into the required theme (and
  requirements).

  2. Description of the design and implementation of the artifact (and how each
  person contributed), focusing on the key concepts and ideas.

  3. Summary of what was accomplished and what was learned.

  4. A demonstration of the artifact.

  Each group should plan for about **6** minutes for the presentation and **2**
  minutes for questions.   Towards the end of each presentation day, there will
  be about **15** minutes for students to go around the room and test out the
  projects presented that day.

  ## Rubric

  The project will be graded with the following rubric:

  <table class="table table-bordered table-striped">
    <thead>
      <th>Metric</th>
      <th>Points</th>
      <th>Description</th>
    </thead>
    <tbody>
      <tr>
        <td>Proposal</td>
        <td>10</td>
        <td>Does the proposed project meet the requirements?</td>
      </tr>
      <tr>
        <td>Progress</td>
        <td>10</td>
        <td>How much work did the group do before the sprint?</td>
      </tr>
      <tr>
        <td>Presentation</td>
        <td>10</td>
        <td>How well did the group present and demonstrate their artifact?</td>
      </tr>
      <tr>
        <td>Deliverable - Requirements</td>
        <td>10</td>
        <td>How well did the project meet the requirements?</td>
      </tr>
      <tr>
        <td>Deliverable - Technical Challenge</td>
        <td>10</td>
        <td>How difficult was the project?</td>
      </tr>
      <tr>
        <td>Deliverable - Execution</td>
        <td>10</td>
        <td>How complete was the project?.</td>
      </tr>
      <tr>
        <td>Deliverable - User Experience</td>
        <td>10</td>
        <td>How enjoyable was project for end users?</td>
      </tr>
    </tbody>
  </table>

  <div class="alert alert-danger" markdown="1">
  #### <i class="fa fa-gavel"></i> Peer Evaluation

  The first two metrics will be graded by the instructor, but the final five
  metrics will be evaluated based on feedback from your fellow classmates.
  </div>

  ## Deliverables

  Once you have organized your group, please fill out the following
  [form](https://goo.gl/forms/gt3MxfmO9FAHOaoc2):

  <div class="text-center">
    <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSdmM5P5wMxuBI-X1T3iaZluD2pc2KS2aUUE_eAuTfjS-JamAg/viewform?embedded=true" width="760" height="500" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>
  </div>

  You can update your submission whenever you have your proposal, code
  repository, and presentation slides.

  [Arduino]:        https://www.arduino.cc/
  [Raspberry Pi]:   https://www.raspberrypi.org/
  [Emulator]:       https://en.wikipedia.org/wiki/Emulator
  [DOSBox]:         https://www.dosbox.com/
  [Python]:         https://www.python.org/
  [PyGame]:         http://www.pygame.org/
  [C]:              https://en.wikipedia.org/wiki/The_C_Programming_Language
  [C++]:            https://isocpp.org/
  [assembly]:       https://en.wikipedia.org/wiki/Assembly_language
  [GitHub]:         https://github.com
  [GitLab]:         https://gitlab.com
  [Bitbucket]:      https://bitbucket.org
  [Zork]:           https://en.wikipedia.org/wiki/Zork
  [Adventure]:      https://en.wikipedia.org/wiki/Adventure_(Atari_2600)
  [NetHack]:        https://en.wikipedia.org/wiki/NetHack
  [Rogue]:          https://en.wikipedia.org/wiki/Rogue_(video_game)
  [SpaceWar]:       https://en.wikipedia.org/wiki/Spacewar!
  [Pong]:           https://en.wikipedia.org/wiki/Pong
  [Pac-Man]:        https://en.wikipedia.org/wiki/Pac-Man
  [Breakout]:       https://en.wikipedia.org/wiki/Breakout_(video_game)
  [Frogger]:        https://en.wikipedia.org/wiki/Frogger
  [QBasic]:         https://en.wikipedia.org/wiki/QBaSIC
  [Nibbles]:        https://en.wikipedia.org/wiki/Nibbles_(video_game)
  [Gorillas]:       https://en.wikipedia.org/wiki/Gorillas_(video_game)
  [Z80]:            https://en.wikipedia.org/wiki/Z80
  [8086]:           https://en.wikipedia.org/wiki/Intel_8086
  [M68K]:           https://en.wikipedia.org/wiki/Motorola_6800
  [FPGA]:           https://en.wikipedia.org/wiki/Field-programmable_gate_array
  [Arduboy]:        https://arduboy.com/
  [DOOM]:           https://en.wikipedia.org/wiki/Doom_(series)
  [MIDI]:           https://en.wikipedia.org/wiki/MIDI
  [BBS]:            https://en.wikipedia.org/wiki/Bulletin_board_system
