title:      "Reading 03: PCs, Nintendo"
icon:       fa-book
navigation: []
internal:
external:
body:       |

  **Everyone**:

  Next week, we will discuss the [1980s in video gaming], where we saw the rise
  of home computing and PC gaming, along with the revival of console gaming
  with the introduction of the [Nintendo].  In class, we will focus on [DOS]
  and video game classics such as [Bomberman], [Castlevania], [Choplifter],
  [Final Fantasy], [King's Quest], [The Legend of Zelda], [Mega Man],
  [Metroid], [Microsoft Flight Simulator], [Prince of Persia], [SimCity], [The
  Oregon Trail], and [Warlords].  Additionall, we will also briefly discuss the
  origins of game development studios such as [Broderbund], [Sierra On-Line],
  and [Electronic Arts].

  [1980s in video gaming]: https://en.wikipedia.org/wiki/1980s_in_video_gaming
  [Nintendo]:           https://en.wikipedia.org/wiki/Nintendo
  [Broderbund]:         https://en.wikipedia.org/wiki/Br%C3%B8derbund
  [Sierra On-line]:     https://en.wikipedia.org/wiki/Sierra_On-Line
  [Electronic Arts]:    https://en.wikipedia.org/wiki/Electronic_Arts
  [Bomberman]:          https://en.wikipedia.org/wiki/Bomberman
  [Castlevania]:        https://en.wikipedia.org/wiki/Castlevania
  [Final Fantasy]:      https://en.wikipedia.org/wiki/Final_Fantasy
  [King's Quest]:       https://en.wikipedia.org/wiki/King%27s_Quest_I
  [The Legend of Zelda]:https://en.wikipedia.org/wiki/The_Legend_of_Zelda
  [Mega Man]:           https://en.wikipedia.org/wiki/Mega_Man_(original_series)
  [Metroid]:            https://en.wikipedia.org/wiki/Metroid
  [Microsoft Flight Simulator]: https://en.wikipedia.org/wiki/Microsoft_Flight_Simulator
  [Prince of Persia]:   https://en.wikipedia.org/wiki/Prince_of_Persia
  [SimCity]:            https://en.wikipedia.org/wiki/SimCity
  [Warlords]:           https://en.wikipedia.org/wiki/Warlords_(game_series)
  [Choplifter]:         https://en.wikipedia.org/wiki/Choplifter
  [The Oregon Trail]:   https://en.wikipedia.org/wiki/The_Oregon_Trail_(video_game)

  ## Readings

  For this week, you are to read the following articles:

  ### Personal Computing

  - [The complete history of the IBM PC, part one: The deal of the century](https://arstechnica.com/gadgets/2017/06/ibm-pc-history-part-1/)

  - [The complete history of the IBM PC, part two: The DOS empire strikes](https://arstechnica.com/gadgets/2017/07/ibm-pc-history-part-2/)

  - [How Early Computer Games Influenced Internet Culture](https://www.theatlantic.com/technology/archive/2016/04/how-early-computer-games-influenced-internet-culture/478167/)

  - [What It's Like to Use an Original Macintosh in 2017](https://www.theatlantic.com/technology/archive/2017/05/mac-attack/527979/)

  Also, feel free to peruse the [Computer History Museum]'s [Personal
  Computers] exhibition.

  ### Nintendo

  - [The Surprisingly Long History of Nintendo](https://gizmodo.com/the-surprisingly-long-history-of-nintendo-1354286257)

  - [The Rise of Nintendo: A Story in 8 Bits](http://grantland.com/features/the-rise-of-nintendo-video-games-history/)

  - [The History of NES](http://www.nintendojo.com/features/the-history-of-nes)

  We will watch the following video in class: [Nintendo and a New Standard for Video Games: Crash Course Games #7](https://www.youtube.com/watch?v=34zvOgLZOY4)

  ### Video Games

  After doing some reading, you should play a few old [DOS], [Apple II], or
  [Macintosh] games using the [Internet Archive]:

  - [Software Library: MS-DOS Games](https://archive.org/details/softwarelibrary_msdos_games)

  - [Software Library: Apple II](https://archive.org/details/softwarelibrary_apple_games)

  - [Software Library: Macintosh](https://archive.org/details/softwarelibrary_mac)

  As you play a game, do some research on the history and story behind the
  game.

  [Internet Archive]:       https://archive.org
  [Computer History Museum]:http://www.computerhistory.org/
  [Personal Computers]:     http://www.computerhistory.org/revolution/personal-computers/17/intro
  [DOS]:                    https://en.wikipedia.org/wiki/MS-DOS
  [Apple II]:               https://en.wikipedia.org/wiki/Apple_II
  [Macintosh]:              https://en.wikipedia.org/wiki/Macintosh

  ## Reflection

  For this upcoming week, you are to consider the following question as you
  perform the readings, discuss in class, and play some old games:

  > How is gaming on a personal computer different from gaming on a video
  console?  What are the advantages and disadvantages of either platform?  What
  sort of games are better suited for either type of system?

  Your individual reflection should provide short reviews of the games you
  played, provided screenshots of gameplay, and ultimately answer this question.
