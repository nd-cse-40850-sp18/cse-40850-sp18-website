CSE 40850 Spring 2018
=====================

This is the source code for the CSE 40850 History of Computing (Spring 2018)
[course website](http://www3.nd.edu/~pbui/teaching/cse.40850.sp18/).
